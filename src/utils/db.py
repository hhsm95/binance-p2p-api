import os
from typing import Optional

import pymysql
from pymysql.connections import Connection
from pymysql.cursors import Cursor


class Database:
    def __init__(self) -> None:
        self.host = os.environ.get("DB_HOST")
        self.port = int(os.environ.get("DB_PORT"))
        self.user = os.environ.get("DB_USER")
        self.password = os.environ.get("DB_PASS")
        self.database = os.environ.get("DB_NAME")
        self.charset = os.environ.get("DB_CHARSET")

        self.conn: Optional[Connection] = None
        self.cursor: Optional[Cursor] = None

    def set_connection(self) -> None:
        self.conn = pymysql.connect(
            host=self.host,
            port=self.port,
            user=self.user,
            password=self.password,
            db=self.database,
            charset=self.charset,
        )

    def set_cursor(self) -> bool:
        if self.conn is None:
            return False

        try:
            self.cursor = self.conn.cursor()
            return True
        except Exception as e:
            print(e)
            self.cursor = None
            return False

    def add_binance_p2p(
        self,
        coin: str,
        price: float,
        tradable_quantity: float,
        min_payment: float,
        max_payment: float,
        nickname: str,
        month_order_count: int,
    ) -> Optional[int]:
        sql = """INSERT INTO BINANCE_P2P (coin, price, tradable_quantity, min_payment, max_payment, nickname, month_order_count)
VALUES (%s, %s, %s, %s, %s, %s, %s)"""
        try:
            self.cursor.execute(
                sql,
                (
                    coin,
                    price,
                    tradable_quantity,
                    min_payment,
                    max_payment,
                    nickname,
                    month_order_count,
                ),
            )
            self.conn.commit()
            return self.cursor.lastrowid
        except Exception as e:
            print(e)
            return None

    def close(self):
        self.conn.close()
