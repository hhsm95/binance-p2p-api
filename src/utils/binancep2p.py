from typing import Dict, List, Optional

from src.classes.http_client import HttpClient


class BinanceP2P:
    def __init__(self) -> None:
        self.hostname = "https://p2p.binance.com"
        self.p2p_url = "/bapi/c2c/v2/friendly/c2c/adv/search"
        self.http_client = HttpClient()

    def get_p2p(self, payload: Dict) -> Optional[List[Dict]]:
        url = f"{self.hostname}{self.p2p_url}"
        resp = self.http_client.post(url, json=payload)
        if resp is None:
            return None

        return resp.json()["data"]
