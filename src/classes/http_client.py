import logging
from typing import Optional

import requests

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class HttpClient:
    def __init__(self) -> None:
        self.session = requests.session()
        self.session.headers[
            "user-agent"
        ] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36"

    def request(
        self, method: str, url: str, **kwargs
    ) -> Optional[requests.models.Response]:
        try:
            return self.session.request(method, url, **kwargs)
        except Exception as e:
            LOGGER.info(e)
            return None

    def get(self, url: str, **kwargs) -> Optional[requests.models.Response]:
        return self.request("GET", url, **kwargs)

    def post(self, url: str, **kwargs) -> Optional[requests.models.Response]:
        return self.request("POST", url, **kwargs)
