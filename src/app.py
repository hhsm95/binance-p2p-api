import math

from src.utils.binancep2p import BinanceP2P
from src.utils.db import Database


def main():
    p2p = BinanceP2P()
    coins = ["USDT", "BUSD"]
    for coin in coins:
        results = p2p.get_p2p(
            {
                "asset": coin,
                "fiat": "MXN",
                "merchantCheck": False,
                "page": 1,
                "payTypes": [],
                "publisherType": None,
                "rows": 10,
                "tradeType": "BUY",
            }
        )

        if not results:
            print("Binance Error")
            continue

        db = Database()
        db.set_connection()
        success = db.set_cursor()

        if not success:
            print("DB Error")
            continue

        row = results[0]
        adv = row["adv"]
        advertiser = row["advertiser"]

        price = float(adv["price"])
        tradable_quantity = float(adv["tradableQuantity"])
        nickname = advertiser["nickName"]
        month_order_count = advertiser["monthOrderCount"]
        min_quantity = float(adv["minSingleTransQuantity"])
        min_payment = math.ceil(price * min_quantity)
        max_payment = math.ceil(price * tradable_quantity)

        inserted_id = db.add_binance_p2p(
            coin,
            price,
            tradable_quantity,
            min_payment,
            max_payment,
            nickname,
            month_order_count,
        )
        db.close()

        if inserted_id is None:
            print("Error saving")
            continue

        print(
            coin,
            price,
            tradable_quantity,
            min_payment,
            max_payment,
            nickname,
            month_order_count,
        )
        print(f"inserted_id: {inserted_id}")
